### Installation

`sudo apt install logstash`

### Run as service

`sudo systemctl enable logstash --now`

### configuration 

- `/etc/logstash/conf.d/.`
<https://www.elastic.co/guide/en/logstash/7.0/config-examples.html>
