# For better verbosity 

`sudo ufw logging on` /var/log/ufw.log

# Status of Firewall

`sudo ufw status`

# Allow some application or port no.

```
sudo ufw allow http ssh
sudo ufw allow 8000/tcp
```

# limit the connection default is 6 per 30 seconds
`sudo ufw limit 22/tcp` this helps in if attackers connect more than 6 connection through `ssh` it will block

# For blocking

`sudo ufw deny 8000/tcp`

# Turn on firewall

`sudo ufw enable`

# See the rule with number
`sudo ufw status numbered`

# Delete rule through number

`sudo ufw delete 8` 8th number rule going to delete

# Enabling the service

`systemctl enable --now ufw.service`

# for more

<https://www.cyberciti.biz/faq/how-to-configure-firewall-with-ufw-on-ubuntu-20-04-lts/>
