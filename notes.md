- blacktech , nexuxiq, sonarcube

# use `declare`
```sh
# Declare an associative array using 'declare'
declare -A colors

# Assign values
colors=(
    [apple]="red"
    [banana]="yellow"
    [cherry]="red"
)

# Access elements
echo "Color of apple: ${colors[apple]}"  # Outputs: red
echo "Color of banana: ${colors[banana]}"  # Outputs: yellow

# Add a new element
colors[date]="brown"
echo "Color of date: ${colors[date]}"  # Outputs: brown
```

# To print something at time of login not a shell login
- `/etc/motd` # `message of the day`
- `/etc/update-motd.d` # you can write a script to print

# Shell `set` Command

The `set` command in the shell is used to change the value of shell options and parameters. It can enable or disable various shell options.

## Syntax

- **Enable an option:** `set -o option_name` or `set -option`
- **Disable an option:** `set +o option_name` or `set +option`

## Common Shell Options

Here are some common shell options that you might encounter:

1. **`allexport`**: Automatically export all variables to the environment.
   - Enable: `set -o allexport` or `set -a`
   - Disable: `set +o allexport` or `set +a`

2. **`nounset`**: Treat unset variables as an error when substituting.
   - Enable: `set -o nounset` or `set -u`
   - Disable: `set +o nounset` or `set +u`

3. **`errexit`**: Exit immediately if a command exits with a non-zero status.
   - Enable: `set -o errexit` or `set -e`
   - Disable: `set +o errexit` or `set +e`

4. **`xtrace`**: Print commands and their arguments as they are executed.
   - Enable: `set -o xtrace` or `set -x`
   - Disable: `set +o xtrace` or `set +x`

## Listing Shell Options

* You can use `set -o` without any arguments to list the current settings of all shell options:

```bash
set -o
```
---

## To restric user to run some command only

* in `/etc/sudoers`
- `avinash ALL=(ALL) NOPASSWD: /usr/bin/apt, /usr/bin/apt-get` user `avinash` can only run `these two command`
- `avinash ALL=(ALL) NOPASSWD: ALL, !/usr/bin/apt, !/usr/bin/apt-get` user `avinash` can run all except `these two`
## To restrict user to connect wifi which is not listed

- `sudo gpasswd -d avi netdev`  # netdev groups are responsible for managing network. So this command remove user from `netdev` groups

### nginx config
- `certbot certonly -v --standalone --register-unsafely-without-email -d rmgtoday.com`
```nginx
server {
    listen 443 ssl;
    server_name pay.domain.in;

    ssl_certificate /etc/letsencrypt/live/pay.domain.in/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/pay.domain.in/privkey.pem;

    ssl_session_cache    shared:SSL:1m;
    ssl_session_timeout  5m;

    ssl_ciphers  HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers  on;

    root   /var/www/html/build;
    index  index.html index.htm;
    location / {
            try_files $uri $uri/ /index.html;
    }
}
```
## load balancing nginx
```nginx
http {
	upstream backend {
		server 10.1.0.101; 
		server 10.1.0.102;
		server 10.1.0.103;
	}
	server {
		listen 443 ssl;
		server_name domain_name;
		ssl_certificate /etc/letsencrypt/live/domain_name/cert.pem;
		ssl_certificate_key /etc/letsencrypt/live/domain_name/privkey.pem;
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

		location / {
			proxy_pass http://backend;
		}
	}
}
```
### mount external drive with wsl
`sudo mount -t drvfs D: /mnt/d` run it in wsl os not on windows
### IF `sh through file not there`
* `readelf -l your_executable | grep "program interpreter"`
* alternate way `ldd executable_filename`

### If you want not to pudh only fetch from remote repo
* `git remote set-url --push origin no_push`
### To sync remote repo branch with local remote/repo branch
* `git remote prune origin` it prune all remote/repo reference which attached with origin remote
### To check `LF` or `CRLF` of file
* `od -c file_name | grep -E (\r|\n)` if `\n` then LF, if `\r\n` then CRLF
### To block the translation of `LF` to `CRLF` by git
* `git config --global core.autocrlf false`

### some vim hacks
- If you have `your.file`
```
var1
var2
var3
```
select region and then `:` type `s/\(.*\)/\L\1=\U\1/`
```
var1=VAR1
var2=VAR2
var3=VAR3
```
select everything which is include $(....)
`!grep -w "\$(.*)" % --only-matching` you can pipe it to file

### check how much memory used by process
* `pmap PID` get `PID` from `pidof name_process`

### limit memory used by process
* `ulimit -v 1000000` this will limit `memory for current shell session` in kb

### set docker rootless
1. `cat /etc/group grep docker` it shows `docker` group available or not
2. `sudo groupadd -f docker` it will add `docker` groups if not available
3. `usermod -aG docker aviansh` it will add `avinash` to docker group
4. `reboot` or `newgrp docker`
5. check `groups`
### gdm3 enable issue
`dpkg-reconfigure gdm3`

### tap to click in GDM
- temporarily
```
sudo su - gdm -s /bin/bash
export $(dbus-launch)
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
```
- permanently
```
/etc/gdm3/greeter.dconf-defaults

[org/gnome/desktop/peripherals/touchpad]
tap-to-click=true
```
### Network manager unmanaged issue in ubuntu server 
- remove `networkd and netplan` packages
- may be you need to change
 ```
/etc/NetworkManager/NetworkManager.conf

#managed=false
managed=true
```

### Network manager disconnect interface status
- `nmcli device connect interface_name`
- `nmcli connection modify ens0p1 connection.autoconnect yes` autoconnect on startup
### For dynamic ip, ping by hostname
```
install avahi-deamon avahi-utils
service avahi-deamon restart

then
ping hostname.local

avahi basically translate hostname to ip-address
```
<https://superuser.com/questions/1609203/cannot-resolve-hostname-updating-hostname-for-machine-on-local-lan-when-attemp/1609217#1609217>

### If Firefox Postman ... not opening with x11forwarding
`export XAUTHORITY=$HOME/.Xauthority`

### if normal-program asking for a password
`rm .local/share/keyrings/*` then not set a password
---

## This is emacs like key-bind for zsh (default)
### Movement:

- `Ctrl+A` or `Home`: Move the cursor to the beginning of the command line (`beginning-of-line`).
- `Ctrl+E` or `End`: Move the cursor to the end of the command line (`end-of-line`).
- `Ctrl+B` or `←`: Move the cursor one character to the left (`backward-char`).
- `Ctrl+F` or `→`: Move the cursor one character to the right (`forward-char`).
- `Alt+B` or `Alt+←`: Move the cursor one word backward (`backward-word`).
- `Alt+F` or `Alt+→`: Move the cursor one word forward (`forward-word`).

### Editing:

- `Ctrl+D` or `Delete`: Delete the character under the cursor (`delete-char`).
- `Ctrl+H` or `Backspace`: Delete the character before the cursor (`backward-delete-char`).
- `Ctrl+W`: Delete the word before the cursor (`backward-kill-word`).
- `Alt+D`: Delete the word after the cursor (`kill-word`).
- `Ctrl+K`: Delete from the cursor position to the end of the line (`kill-line`).
- `Ctrl+U`: Clear the entire command line (`backward-kill-line`).
- `Ctrl+Y`: Yank (paste) the most recently killed text (`yank`).
- `Ctrl+T`: Transpose characters (swap the character before the cursor with the character under the cursor) (`transpose-chars`).
- `Alt+T`: Transpose words (swap the word before the cursor with the word after the cursor) (`transpose-words`).

### History:

- `Ctrl+P` or `↑`: Move to the previous command in history (`up-history`).
- `Ctrl+N` or `↓`: Move to the next command in history (`down-history`).
- `Ctrl+R`: Search backward in history for a command (`history-incremental-search-backward`).
- `Ctrl+S`: Search forward in history for a command (`history-incremental-search-forward`).

### Miscellaneous:

- `Ctrl+J` or `Enter`: Execute the current command line (`accept-line`).
- `Ctrl+L`: Clear the screen (`clear-screen`).
- `Ctrl+C`: Cancel the current command or prompt (`send-break`).
- `Ctrl+Z`: Suspend the current process (`push-line`).

---

# CURL
---
## CURL like website
1. curl parrot.live
2. wttr.in/city
3. ipinfo.io
4. rate.sx

---

### Download file with `curl`
* `curl -o localfile_name URL` this will download remote file with `localfile_name`
* `curl -O URL` this will download file as remote_name
* `curl -LO URL` this will work better because it support `--location` if `http` has redirection `URL`

## FTP through curl

### Create one directory on remote

`curl -Q "MKD dir_name" -u user:pass ftp://localhost/dir1` this will create `dir_name` in `dir1` directory

### Create directory (missing) on remote and then upload `file.txt`

`curl --ftp-create-dirs -T file.txt -u username:password ftp://localhost/dir1/dir2/dir3/dir4/ ` this will create `parent` and `child` derectory also
### Upload multiple file
* `curl -T "{file1.txt,file2.txt}" ftp://localhost/ --user user:pass` do not put <space> after `,`
* `curl -T *.txt ftp://localhost/ --user user:pass` regex support
* `curl -T 'image[1-99].jpg' ftp://localhost/upload/ -u user:pass`

### List down the file and directory from remote
`curl -u username:password -l ftp://ftp.example.com/remote/directory/`

### use method with curl
`curl -X POST -u username:password -H "Content-Type: application/json" -d '{"key1":"value1","key2":"value2"}' https://example.com/api/endpoint`

### Avinash

- [x] Mqtt and similar brokers like kafka specifically emqx
- [x] Containerisation and docker containers
- [x] Kubernetes basics
- [x] Postgres/ mysql/mssql
- [ ] CDC for databases
- [ ] Trino /presto sql
- [x] AWS s3,iam,ec2,ecs,vpc,eks
- [x] Terraform/cloudformation
- [x] Jenkins
- [x] Graphana
- [x] ELK
- [x] Redis


