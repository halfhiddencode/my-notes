# Build image
`docker build -t hall:latest .`
# run image
`docker run -it --name halfhidd -p 8080:7070 -v local_LOCATION:CUSTOM_LOCATION_IN_CONTAINER hall:latest`
# save image in tar format 
`docker save imagename:v1 > image.tar` only tar is supported
# load image from tar
`docker load < image.tar`
# see process
`docker ps -a`
# Entering into container
`docker exec -it CONTAINER_ID COMAND`
# start exited container
`docker start CONTAINER_ID bash`
# run exact copy of container configuration
`docker commit previousconatiner newcontainer`
# copy folder host to container
`docker cp src/. container_id:/target`
# getting only container id of process
`docker ps -q`
# removing docker container which is exited state
`docker ps --filter status=exited -q | xargs docker rm`
# To see IP,volume,etc of Container
`docker inspect CONTAINER_ID | grep IPAdrress`
# to get IP in formate
`docker inspect -f "{{.Name}} {{range.NetworkSettings.Networks}} {{.IPAddress}}{{end}}" CONTAINER_ID`

---

# CREATE NETWORK
* driver is bridge
`docker network create -d bridge my-bridge-net`
* driver is overlay i.e. swarm and also attachable to any services
`docker network create -d overlay --attachable my-custom-net`
# To see network
`docker network ls`

---

# Swarm Services
* init swarm
`docker swarm init`
* Join worker
`docker swarm join --token......HASH` 
# Create Swarm Services
`docker service create -d -p 7080:8080 --replica=5 --mount type=bind,src=LOCATION-OF-DIRECTORY,dst=LOCATION_INTO_CONATINER IMAGE_NAME`
# Create service on specific node
` docker service create --constraint="node.role==manager" -d -p 5080:3000 --network=my-bridge IMAGE_NAME`
# Scale the services
`docker service scale name-of-the-service=10` **not possible to scale if it is running on global mode**
# see service
`docker service ls`
# To see which process running on which node and state also
`docker service ps SERVICE_ID`
# To get id_of_SWARM_services which is in running state
`docker service ps --filter desired-state=running SERVICE_ID`
# filtering two at a time
`docker servic ps --filter desired-state=running --filter node=manager SERVICE_ID`
# to get container id of swarm services
`docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' ps_id_of_SWARM_services`
## how to run gui application in docker
1. `xhost +`
2. `-v /tmp/X11-unix:/tmp/X11-unix` to container
3. `set DISPLAY=$DISPLAY`
4. run favorite application `geany`

## connection through internet
* for HTTP 80
* for HTTPS 443
