# Installation

`sudo apt install kibana`

# Configure Kibana

### sudo nano /etc/kibana/kibana.yml

```
#server.port: 5601
#server.host: "your-hostname"
#elasticsearch.hosts: ["http://localhost:9200"]
server.port: 5601
server.host: "localhost"
elasticsearch.hosts: ["http://localhost:9200"]
```
### If firewall is running

`sudo ufw allow 5601/tcp` to allow kibana port

### Run Kibana service

`sudo systemctl enable --now kibana`

### Access frontend from web browser

- at url `http://localhost:5601`

### Need to set kibana_system user password in elasticsearch

`/usr/share/elasticsearch/bin/elasticsearch-reset-password -u kibana_system -i`
- alternate way with token
`/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana` and paste it in <http://localhost:5601> token area
- enter `elastic` user and password `elastic`

### For verification code run

`/usr/share/kibana/bin/kibana-verification-code`

