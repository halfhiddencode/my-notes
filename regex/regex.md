# character class and character ranges

## search `grey` or `gray`
- `grep "gr[ae]y" file_name` **NOTE:** in regex it looks like [g][r][ae][y], in [] it represent if `one char` is matched
- if you have case `grep "[Gg]r[ae]y" file_name`

## serch if any string contains number

- `grep "[0123456789]" file_name`
- `grep "[0-9]" file_name` another way

## serch if any string contains alphabets

- `grep "[a-z]" file_name`
- `grep "[A-Z]" file_name`
- `grep "[a-zA-Z]" file_name` it will match all lower and uppercase same time

# negation character class

- `grep "[^ae]" file_name` this will exclude `a or e` from all string
- **NOTE:** if you use `[ae^]` this will search `a or e` in all string 

# Alternation

- `grep -oP "(gray|grey)" file_name` we need to use Perl-compatible regular expressions (PCREs) i.e `-P` and `-o` for only matching are printed
- `grep -oP "(a|e) file_name"` this will print `letter` because it will match letter only
- **NOTE:** you can also use `-E` instead of `-P`, `-E` for POXIS standard

# Quantifiers

- if we see some `pattern` repeating in string
> `ll` comes one times in `hello` and `l` comes two times in `hello`

- `grep -E "l{1,}" file_name` this will find `l` must be present atleast one times in the string
- `grep -E "l{2,6}" file_name` this will find `l` must be present `2<= no. of l <=6` in the string
- `grep -E "l{,6}" file_name` this will find `l` must be present `no. of l <=6` times in the string

## another way

- `grep "l*" file_name` means `l{0,infinity}`
- `grep "l+" file_name` means `l{1,infinity}`
- `grep "l?" file_name` means `l{0,1}`

# Character Escaping

- 
