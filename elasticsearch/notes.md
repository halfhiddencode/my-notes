# Installation on debian

### Download and install the public signing key:

`wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg`

### You may need to install the apt-transport-https package on Debian before proceeding:

`sudo apt-get install apt-transport-https`

### Save the repository definition to /etc/apt/sources.list.d/elastic-8.x.list:

`echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list`

### You can install the Elasticsearch Debian package with:

`sudo apt-get update && sudo apt-get install elasticsearch`

# Configuration

### sudo nano /etc/elasticsearch/elasticsearch.yml

```
#network.host: 192.168.0.1
network.host: localhost

#http.port: 9200
http.port: 9200

# in discovery section
discovery.type: single-node
#cluster.initial_master_nodes: ["avinash"]
#cluster.initial_master_nodes should be commented because of line no. 31 of notes
```

### sudo nano /etc/elasticsearch/jvm.options

```
# Xms represents the initial size of total heap space
-Xms512m
# 512m means 512mb, 4g means 4gb
# Xmx represents the maximum size of total heap space
-xmx512m
```

### Run the elasticsearch

`sudo systemctl enable elasticsearch.service --now`

### Reset password

`/usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic -i` -i for interactive -u for which user

### access frontend from browser
- at url `https://localhost:9200`
- insert username `elastic` password `new-reset-password`

### troubleshooting
- check `/var/log/elasticsearch/elasticsearch.log`
