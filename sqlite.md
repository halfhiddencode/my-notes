### how to run .sql file with sqlite
### There are many way
1. Through sqlite shell  
`sqlite3> .read example.sql`
2. from outside the sqlite shell without connecting to sqlite  
`sqlite3 test.db < example.sql` it will make `test.db` file on present `direcotry`
3. and also by `sqlite3 < example.sql`
### if you want to access `test.db` file
1. `sqlite3 test.db`
2. then in a prompt `sqlite3> .table` to see table name
3. then you can use sql statement **Dont forget `;` at the end of statement** `sqlite3> SELECT * FROM customers;`
### change mode of printing
`sqlite3> .mode markdown` or `.mode column` or `.mode csv`
##### you can also specify in `~/.sqliterc` file
