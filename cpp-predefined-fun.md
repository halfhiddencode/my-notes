### sort array `<algorithm.h>`
```cpp
sort(arr,arr+n); // for ascending order
sort(arr,arr+n,greater<int>()); // for ascending order
```
### string
```cpp
string str="hall"
char ch[]="hall"
// for length two different method
str.size();
strlen(ch);
```
### string to integer
```cpp
int temp=stoi();
```
### operation with vector `<algorithm.h>`
```cpp
sort(v.begin(),v.end()); // O(n)
reverse(v.begin(),v.end()); // O(n)
```
### vector `<vector>`
```cpp
v1.push_back(8); // put 8 in last
v2D.push_back(v1); // we can also assign list into 2d matrix by *push_back()*
v1.pop_back(); //remove one last element
v1.erase(itr); // O(n) it take iterator as input not value
v1.erase(startPosition,endPosition); // O(n) it can also take range address
v1.clear(); // O(n)
vector<pair<int,int>> vecPai; //decleration of vector of pair
vecPai.push_back(make_pair(4,6)); //Insert data in vectorPair
```
### Priority Queue
```cpp
priority_queue <int> que; // decleration, it store data in descending order (MaxHeap)
priority_queue <int,vector<int>,greater<int>> que; // declaration, ascending order (MinHeap)
// function
que.push(5);
que.empty();
que.pop();
que.top();
que.swap();
que.size();
```
### unorder_map `<unordered_map>`
```cpp
unorder_map<int,int>> map1; //declaration of unorder_map
map.insert({key,value});
map.find(key) // it give iterator i.e address if match otherwise, it give map.end()
map.end(); // it gives address of next of the last i.e no element there
unordered_map<int,int>::iterator iter; //decleration
iter=map.find(key);
value=*iter;
// for hashing 
for(int i=0;i<n;i++)
{
	map1[arr[i]]++; // it will count frequency because if arr[i]=8 then map1[8] will increase
}
// you can also do with a array
int hasharr[/*put the highest number which is present in a given array*/];
for(int i=0;i<n;i++)
{
	hasharr[arr[i]]++;
}
```
