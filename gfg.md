### Function to reverse every sub-array group of size k.
```cpp
void reverseInGroups(vector<long long>& arr, int n, int k){
	for(int i=0;i<n;i+=k){
		int left=i;
		int right=min(i+k-1,n-1);
		while(left<right){
			swap(arr[left++],arr[right--]);
		}
	}
}
```
### Function to check if two arrays are equal or not.
```cpp
bool check(vector<ll> A, vector<ll> B, int N){
	sort(A.begin(),A.end());
	sort(B.begin(),B.end());
	for(int i=0;i<N;i++){
		if(A[i]!=B[i])
			return false;
	}
	return true;
}
```
### Convert array into Zig-Zag fashion
```cpp
void zigZag(int arr[], int n){
	int i=0;
	while(i<n-1){
		if(i%2==0&&arr[i]>arr[i+1])
			swap(arr[i],arr[i+1]);
		else
		if(i%2==1&&arr[i]<arr[i+1])
			swap(arr[i],arr[i+1]);
		i++;
	}
}
```
### Minimize the sum of product
```cpp
long long int minValue(int a[], int b[], int n){
	long long int s=0;
	sort(a,a+n);
	sort(b,b+n,greater<int>());
	for(int i=0;i<n;i++){
		s=s+a[i]*b[i];
	}
	return s;
}
```
### First element to occur k times
```cpp
int firstElementKTime(int a[], int n, int k){
	int flag=0;
	unordered_map <int,int> map1;
	for(int i=0;i<n;i++){
		map1[a[i]]++; // for frequency off element
		if(map1[a[i]]==k){
			flag=1;
			return a[i];
		}
	}
	if(flag==0)
	{
		return -1;
	}
}
```
### Subarray with given sum
```cpp
vector<int> subarraySum(int arr[], int n, long long s){
	long long currSum=0;
	vector<int> temp;
	int start=0,flag=0;
	for(int i=0;i<n&&flag==0;i++){
		while(currSum>s&&start<i){
			currSum=currSum-arr[start];
			start++;
		}
		if(currSum==s){
			flag=1;
			temp.push_back(start+1);
			temp.push_back(i);
			return temp;
		}
		if(i<n)
			currSum=currSum+arr[i];
	}
	if(flag==0){
		temp.push_back(-1);
		return temp;
	}
}
```
### missing number in array
```cpp
int MissingNumber(vector<int>& array, int n){
	unordered_map <int,int> map;
	for(int i=0;i<n-1;i++){
		map[array[i]]++;
	}
	map[0]=1;
	for(int i=1;i<=n;i++){
		if(map[i]!=1);
	}
}
// Another approach by sumation fo sries
int MissingNumber(vector<int>& array, int n){
	long int sum=n*(n+1)/2;
	for(int i=0;i<n-1;i++){
		sum=sum-array[i];
	}
	return sum;
}
```
### Sort an array of 0s, 1s and 2s 
```cpp
void sort012(int a[], int n){
	unordered_map<int,int> map;
	for(int i=0;i<n;i++){
		map[a[i]]++;
	}
	for(int i=0;i<map[0];i++){
		a[i]=0;
	}
	for(int i=map[0];i<map[0]+map[1];i++){
		a[i]=1;
	}
	for(int i=map[0]+map[1];i<n;i++){
		a[i]=2;
	}
}
```
### Equilibrium Point
```cpp
int equilibriumPoint(long long a[], int n){
	long long sum[n];
	sum[0]=a[0];
	for(int i=1;i<n;i++){
		sum[i]=sum[i-1]+a[i];
	}
	if(n>2)
	for(int i=0;i<n;i++){
		long long leftSum=sum[i]-a[i];
		long long rightSum=sum[n-1]-sum[i];
		if(leftSum==rightSum)
			return i+1;
	}
	else
		if(n==1)
			return 1;
	return -1;
}
```
### Function to find the leaders in the array.
```cpp
vector<int> leaders(int a[], int n){
	vector<int> leader;
	int maxFromRight;
	maxFromRight=a[n-1];
	leader.push_back(maxFromRight);
	for(int i=n-2;i>=0;i--){
		if(maxFromRight<=a[i]){ // condition need to added two equal element so, <=
			maxFromRight=a[i];
			leader.push_back(maxFromRight);
		}
	}
	reverse(leader.begin(),leader.end());
	return leader;
}
```
### Largest subarray with 0 sum
```cpp
int maxLen(vector<int>&A, int n){
	unordered_map<int, int> map;
	int sum = 0;
	int maxlen = 0;
	for(int i=0;i<n;i++){
		sum += A[i];
		if(A[i]==0&&maxlen==0)
			maxlen=1;
		if(sum==0)
			maxlen=i + 1;
		if (map.find(sum) != map.end()) // check key is present or not
			maxlen = max(maxlen, i - map[sum]);
		else
			map[sum] = i;
	}
	return maxlen;
}
```
### Array Subset of another array
```cpp
string isSubset(int a1[], int a2[], int n, int m){
    unordered_set <int> s;
    for(int i=0;i<n;i++){
        s.insert(a1[i]);
	}
	int p = s.size(); // size before 2nd set
    for(int i=0;i<m;i++)
        s.insert(a2[i]);
    if(s.size()==p) // size after 2nd set because every set are unique thats why it contain no duplicate
        return "Yes";
    else
        return "No";
// Another method;
    int count;
    unordered_set <int> s;
    for(int i=0;i<n;i++)
        s.insert(a1[i]);
    for(int i=0;i<m;i++)
        if(s.find(a2[i])!=s.end())
            count++;
    if(count==m)
        return "Yes";
    else
        return "No";
}
```
### Longest Common "Prefix" in an Array
```cpp
string longestCommonPrefix (string arr[], int N){
	string prefix = arr[0];
	for(int i=1;i<N;i++){
		prefix = logestCompare(prefix,arr[i]); // passing prefix of previous two string
	}
	if(prefix!="") //check prefix is empty or not
		return prefix;
	return "-1";
}
/* this is function for check longest prefix in two given string*/
string logestCompare(string s1,string s2){ 
	string result=""; // if not match it will return empty string
	int n=min(s1.length(),s2.length()); // check min length of string
	for(int i=0;i<n;i++){
		if(s1[i]!=s2[i]) // just compare whenever not match loop breaks
			break;
		result+=s1[i];
	}
	return result;
}
```
### Swapping pairs make sum equal
```cpp
int findSwapValues(int A[], int n, int B[], int m){
	long sum1=0,sum2=0;
	sort(A,A+n);
	sort(B,B+n);
	for(int i=0;i<n;i++){
	sum1 = sum1 + A[i];
	}
	for(int i=0;i<m;i++){
	sum2=sum2+B[i];
	}
	if((sum1-sum2)%2==0){
		int i=0,j=0;
		int target=(sum1-sum2)/2;
		while(i<n&&j<m){
			int diff = A[i] - B[j];
			if(diff==target)
				return 1;
			else
				if(diff<target)
					i++;
				else
					if(diff>target)
						j++;
		}
		return -1;
	}
	else
		return -1;
}
```
### Find all pairs with a given sum
```cpp
vector<pair<int,int>> allPairs(int A[], int B[], int N, int M, int X)
{
	vector<pair<int,int>> result;
	unordered_set <int> set;
	for(int i=0;i<N;i++){
		set.insert(A[i]);
	}
	for(int i=0;i<M;i++){
		auto itr=set.find(X-B[i]);
		if(itr!=set.end())
			result.push_back(make_pair(*itr,B[i]));
	}
	sort(result.begin(),result.end());
	return result;
}
```
### Kth smallest element
```cpp
int kthSmallest(int arr[], int l, int r, int k){
	priority_queue<int> q; // declaration of priority_queue
	for(int i=0;i<=r;i++){
		q.push(arr[i]); // insert value in priority_queue
	}
	for(int i=0;i<=r-k;i++){
		q.pop(); // it remove elements all larger than k th
	}
	return q.top(); // it return kth value
}
```
### Zero Sum Subarrays
```cpp
ll findSubarray(vector<ll> arr, int n ){
  ll sum=0,count=0;
  unordered_map <ll,ll> m;
  for(int i=0;i<n;i++){
    sum+=arr[i];
    if(sum==0)
      count++; // when sum is zero and element it self zero then we count it two
    if(m.find(sum)!=m.end()){
      count+=m[sum]; // this will increase count by how much map contains
      m[sum]++; // it will increase map values
    }
    else
      m.insert({sum,1});
  }
  return count;
}
```
### Kadane's Algorithm
```cpp
long long maxSubarraySum(int arr[], int n){
	long long Maxsum=arr[0]; // to set lower limit if element are negative
	int currSum=0;
	for(int i=0;i<n;i++){
		currSum+=arr[i];
		if(currSum>Maxsum)
			Maxsum=currSum;
		if(currSum<0) // if current sum are negative then start with zero
			currSum=0;
	}
	return Maxsum;
}
```
### Minimize the Heights II
```cpp
int getMinDiff(int arr[], int n, int k){
	sort(arr,arr+n); // at first sort it if it is not
	int ans=arr[n-1]-arr[0]; // difference between longest and shortest
	for(int i=0;i<n-1;i++){
		if(arr[i+1]-k >= 0){ // if not negative
			// X = arr[i]; Y = arr[i+1]; this is arbitrary where we can add K from smaller and substract k from lager
			int newMax=max(arr[i]+k,arr[n-1]-k);
			int newMin=min(arr[0]+k,arr[i+1]-k);
			ans=min(ans,(newMax-newMin));
		}
	}
	return ans;
}
```
### Merge Sort
```cpp
#include<iostream>

using namespace std;
#define SIZE 5
void mergesort(int [],int,int);
void merge(int*,int,int,int);
int main()
{
    int arr[SIZE];
    for(int i=0;i<SIZE;i++)
    {
        cin>>arr[i];
    }
    mergesort(arr,0,SIZE-1);
    cout<<endl<<"****************Output*************"<<endl;
    for(int i=0;i<SIZE;i++)
    {
        cout<<" "<<arr[i];
    }
}

void mergesort(int arr[],int l,int r)
{
    int mid=(l+r)/2;
    if(l<r) // this will run untill one element if left after dividing
    {
        mergesort(arr,l,mid); // this divide array in 1st half
        mergesort(arr,mid+1,r); // this divide array in 2nd half
        merge(arr,l,mid,r); // this is merge function
    }
}

void merge(int arr[],int l,int mid,int r)
{
    int temp[r+1]; // to store merge array
    int i=l,k=l,j=mid+1;
    while(i<=mid&&j<=r) // untill one of them are exhausted
    {
        if(arr[i]<arr[j])
        {
            temp[k]=arr[i];
            i++;
        }
        else
        {
            temp[k]=arr[j];
            j++;
        }
        k++; // this increase index to next temp space
    }
    while(j<=r)
    {
        temp[k]=arr[j];
        j++;
        k++;
    }
    while(i<=mid)
    {
        temp[k]=arr[i];
        i++;
        k++;
    }
    for(int i=l;i<=r;i++) // copy all temp element into arr
    {
        arr[i]=temp[i];
    }
}
```
### Selection Sort
```cpp
#include<iostream>

using namespace std;

#define SIZE 5
void selectionSort(int *,int);

int main()
{
    int arr[SIZE];
    cout<<"********** Array Input *************"<<endl;
    for(int i=0;i<SIZE;i++)
    {
        cin>>arr[i];
    }
    cout<<"********** Print Sorted Array ***************"<<endl;
    selectionSort(arr,SIZE);
    for(int i=0;i<SIZE;i++)
    {
        cout<<" "<<arr[i];
    }
}

void selectionSort(int arr[],int n)
{
    for(int i=0;i<n;i++)
    {
        int minindex=i;
        for(int j=i+1;j<n;j++)
        {
            if(arr[minindex]>arr[j]) // to select index of min element
            {
                minindex=j;
            }
        }
        swap(arr[i],arr[minindex]);
    }
}
```
### Minimum number of jumps O(n)
```cpp
int minJumps(int arr[], int n){
  int maxReach=arr[0]; // initial tendency to reach
  int step=arr[0]; // where it is currently
  int jump=1; // atleast one jump
  if(n==1)
  return 0; // minimum number of jump is zero
  else
    if(arr[0]==0)
        return -1; // it doesn't go anywhere
  else{
    for(int i=1;i<n;i++){
      if(i==n-1){
        return jump; // it is at last 
      }
      maxReach=max(maxReach,i+arr[i]); max tendency to go
      step--; // it is decrease gap between current and maxx tendency
      if(step==0){ // i is reaches at max tendency
        jump++;
        if(i>=maxReach){ // max tendency is does not over come current position
          return -1;
        }
        step=maxReach-i; // how much gap between maxx tendency and current position
      }
    }
  }
}
```
### Rat in a maze
```cpp
vector<string> findPath(vector<vector<int>> &m, int n) {
	// Your code goes here
	vector<string> ans;
	vector<vector<bool>> visited(n,vector<bool>(n,0));
	string path="";
	if(m[0][0]==0)
		return ans;
	solve(0,0,m,n,ans,visited,path);
	return ans;
}
bool isSafe(int newx,int newy,vector<vector<bool>> &vis,vector<vector<int>> &m,int n)
{
	if(newx>=0&&newx<n&&newy>=0&&newy<n&&vis[newx][newy]==0&&m[newx][newy]==1)
	{
		return true;
	}
	else
		return false;
}
void solve(int x,int y,vector<vector<int>> &m,int n,vector<string> &ans,vector<vector<bool>> &vis, string path)
{
	//base case
	if(x==n-1&&y==n-1)
	{
		ans.push_back(path);
		return;
	}
	//down movement
	if(isSafe(x+1,y,vis,m,n))
	{
		vis[x][y]=1;
		solve(x+1,y,m,n,ans,vis,path+'D');
		vis[x][y]=0;
	}
	//left
	if(isSafe(x,y-1,vis,m,n))
	{
		vis[x][y]=1;
		solve(x,y-1,m,n,ans,vis,path+'L');
		vis[x][y]=0;
	}
	//right
	if(isSafe(x,y+1,vis,m,n))
	{
		vis[x][y]=1;
		solve(x,y+1,m,n,ans,vis,path+'R');
		vis[x][y]=0;
	}
	//up
	if(isSafe(x-1,y,vis,m,n))
	{
		vis[x][y]=1;
		solve(x-1,y,m,n,ans,vis,path+'U');
		vis[x][y]=0;
	}
}
```
### N Queen
```cpp
bool isSafe(int row, int col, vector<vector<int>> &board)
{
	int y=col;
	int x=row;
	//row
	while(y>=0)
	{
		if(board[x][y]==1)
			return false;
		y--;
	}
	//diagonal
	y=col;
	x=row;
	while(x>=0&&y>=0)
	{
		if(board[x][y]==1)
		{
			return false;
		}
		x--;
		y--;
	}
	y=col;
	x=row;
	while(x<board.size()&&y>=0)
	{
		if(board[x][y]==1)
		{
			return false;
		}
		x++;
		y--;
	}
	return true;
}
void solve(int col,vector<vector<int>> &board,vector<vector<int>> &result,vector<int> &comb)
{
	if(col==board.size())
	{
		result.push_back(comb); // to store comb vector in result matrix
		return;
	}
	for(int row=0;row<board.size();row++)
	{
		if(isSafe(row,col,board))
		{
			board[row][col]=1;
			comb.push_back(row+1);
			solve(col+1,board,result,comb);
			// for backtracking
			board[row][col]=0;
			comb.pop_back();
		}
	}
}
vector<vector<int>> nQueen(int n) {
	// code here
	vector<vector<int>> board(n,vector<int>(n,0));
	vector<vector<int>> result;
	vector<int> comb;
	solve(0,board,result,comb);
	return result;
}
```
### Suduko
```cpp
bool isSafe(int row,int col,int grid[N][N],int val)
{
	for(int i=0;i<N;i++)
	{
		if(val==grid[i][col]) // checking row
			return false;
		if(val==grid[row][i]) // checking col
			return false;
		if(grid[3*(row/3)+i/3][3*(col/3)+i%3]==val) // checking 3X3 matrix
		{
			return false;
		}
	}
	return true;
}

bool SolveSudoku(int grid[N][N])  
{ 
	for(int row=0;row<N;row++)
	{
		for(int col=0;col<N;col++)
		{
			if(grid[row][col]==0)
			{
				for(int val=1;val<=9;val++)
				{
					if(isSafe(row,col,grid,val))
					{
						grid[row][col]=val;
						if(SolveSudoku(grid)) // if solvable
						{
							return true;
						}
						else
							grid[row][col]=0; //backtracking
					}
				}
				return false; // solution doesnt exist
			}
		}
	}
	return true;
}
```
### check palindrome ignoring special character
```cpp
bool sentencePalindrome(string s) 
{
	// code here 
	int start=0,end=s.length()-1;
	while(start<end)
	{
		if(!(s[start]>='a'&&s[start]<='z'))
		{
			start++;
		}
		else
			if(!(s[end]>='a'&&s[end]<='z'))
			{
				end--;
			}
			else
				if(s[start]!=s[end])
				{
					return 0;
				}
				else
				{
					start++;
					end--;
				}
	}
	return 1;
}
```
### Function to reverse words in a given string.
```cpp
string reverseWords(string s) 
{ 
	string temp="";
	stack<string> final;

	for(int i=0;s[i]!='\0';i++)
	{
		if(s[i]=='.')
		{
			final.push(temp);
			temp.erase(temp.begin(),temp.end()); 
			// or temp="";
		}
		else
			temp+=s[i]; // we can added string like math not with char array
	}
	s=""; // empty first then add
	if(temp!="")
		s+=temp; // for last word
	while(!final.empty())
	{
		s+='.';
		s+=final.top();
		final.pop();
	}
	return s;
} 
```
